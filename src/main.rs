use reqwest::blocking::Client;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    prefixes: HashMap<String, String>,
    data: Data,
}

#[derive(Debug, Serialize, Deserialize)]
struct Data {
    bin: Vec<String>,
    bin_groupids: Vec<String>,
    mapm: Vec<String>,
    latex: Vec<String>,
    folder: HashMap<String, String>,
    raw: HashMap<String, String>,
}

#[derive(Deserialize, Debug)]
struct Repo {
    http_url_to_repo: String,
    name: String,
}

use colour::*;

use std::collections::HashMap;
use std::fs;
use std::io;
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::Command;

fn parse_toml() -> Config {
    match fs::read_to_string(dirs::config_dir().unwrap().join("bootstrap").join("config.toml")) {
        Ok(string) => toml::from_str(&string).expect("Could not parse `config.toml`"),
        Err(_) => panic!("Could not find ~/.config/bootstrap/config.toml")
    }
}

fn main() {
    let config: Config = parse_toml();
    let parse_url = |url: &str| -> String {
        let mut string = String::from(url);
        for (key, val) in &config.prefixes {
            string = string.replace(&["<", key, ">"].concat(), val);
        }
        string
    };
    let parse_raw_url = |url: &str| -> String {
        let mut url: &str = &parse_url(url);
        if url.len() > 4 && &url[url.len() - 4..] == ".git" {
            url = &url[..url.len() - 4];
        }
        let filename = match url.rfind('/') {
            Some(idx) => String::from(&url[idx + 1..]),
            None => panic!("Invalid URL"),
        };
        format!("{url}/-/raw/master/{filename}")
    };

    // raw

    green_ln!("Shell scripts (`bin`)");

    let bin_dir = dirs::home_dir().unwrap().join("bin"); // where binaries are symlinked to; this should be in $PATH

    if !bin_dir.is_dir() {
        fs::create_dir_all(&bin_dir).expect("Could not create `~/bin`");
    }

    let client = Client::builder().build().unwrap();

    let repos = |id: &str| -> Vec<Repo> {
        client
            .get(&format!("https://gitlab.com/api/v4/groups/{id}/projects"))
            .send()
            .unwrap()
            .json::<Vec<Repo>>()
            .unwrap()
    };

    let sync_raw = |url: &str, dest: &Path, chmod_ux: bool| {
        let mut filename = match parse_url(url).rfind('/') {
            Some(idx) => String::from(&parse_url(url)[idx + 1..]),
            None => parse_url(url),
        };
        if filename.len() >= 4 && &filename[filename.len() - 4..] == ".git" {
            filename = String::from(&filename[..filename.len() - 4]);
        }

        let output = Command::new("curl")
            .args(["-o", dest.to_str().unwrap(), &parse_raw_url(url)])
            .output()
            .expect("Failed to execute `curl`, make sure you have an internet connection");
        if chmod_ux {
            let mut permissions = dest
                .metadata()
                .unwrap_or_else(|_| panic!(
                    "Failed to set permissions `u+x` for `{}` at {dest:?}",
                    &parse_url(url)
                ))
                .permissions();
            permissions.set_mode(0o644);
        }
        println!("`{}`: ", &filename);
        io::stderr().write_all(&output.stdout).unwrap();
        io::stderr().write_all(&output.stderr).unwrap();
    };

    for url in &config.data.bin {
        let dest: std::path::PathBuf = match parse_url(url).rfind('/') {
            Some(idx) => bin_dir.join(&parse_url(url)[idx + 1..]),
            None => bin_dir.join(&parse_url(url)),
        };
        sync_raw(url, &dest, true);
    }

    for id in config.data.bin_groupids {
        for repo in repos(&id) {
            sync_raw(&repo.http_url_to_repo, &bin_dir.join(&repo.name), true);
        }
    }

    green_ln!("Syncing raw contents through curl (`raw`)");

    for (dest, url) in config.data.raw {
        let dest = &format!("{}", shellexpand::tilde(&dest));
        let dest = Path::new(dest);
        sync_raw(&url, dest, false);
    }

    // folder; will be used for mapm, latex, etc

    let git_pull = |url: &str, dest: &Path| {
        let output = match dest.is_dir() {
            true => Command::new("env")
                .args(["-C", dest.to_str().unwrap(), "git", "pull"])
                .output()
                .expect("Failed to execute `git pull`, make sure you have git"),
            false => Command::new("git")
                .args([
                    "clone",
                    &[&parse_url(url), ".git"].concat(),
                    dest.to_str().unwrap(),
                ])
                .output()
                .expect("Failed to execute `git clone`, make sure you have git"),
        };

        blue!("{:?}: ", dest);
        io::stdout().write_all(&output.stdout).unwrap();
        io::stderr().write_all(&output.stderr).unwrap();
    };

    // mapm

    green_ln!("mapm templates (`mapm`)");

    let texmf = dirs::home_dir()
        .unwrap()
        .join("texmf")
        .join("tex")
        .join("latex");
    let template_dir = dirs::config_dir().unwrap().join("mapm").join("templates");

    if !texmf.is_dir() {
        fs::create_dir_all(&texmf).expect("Could not create `~/texmf/tex/latex`");
    }

    if !template_dir.is_dir() {
        fs::create_dir_all(&template_dir).expect("Could not create `~/.config/mapm/templates`");
    }

    for template in config.data.mapm {
        let dir = match parse_url(&template).rfind('/') {
            Some(idx) => String::from(&parse_url(&template)[idx + 1..]),
            None => parse_url(&template),
        };

        print!("`{}`: ", &dir);

        git_pull(&parse_url(&template), &texmf.join(&dir));

        if !template_dir.join(&dir).exists() {
            Command::new("ln")
                .args([
                    "-s",
                    texmf.join(&dir).join("mapm").to_str().unwrap(),
                    template_dir.join(&dir).to_str().unwrap(),
                ])
                .output()
                .expect("Failed to execute `ln -s`");
        }
    }

    // latex
    // We can reuse `texmf` var from mapm section

    green_ln!("Latex packages and classes (`latex`)");

    for template in config.data.latex {
        let dir = match parse_url(&template).rfind('/') {
            Some(idx) => String::from(&parse_url(&template)[idx + 1..]),
            None => parse_url(&template),
        };
        git_pull(&template, &texmf.join(&dir));
    }

    // folder

    for (dest, url) in config.data.folder {
        let dest = &format!("{}", shellexpand::tilde(&dest));
        let dest = Path::new(dest);
        git_pull(&url, dest);
    }
}
